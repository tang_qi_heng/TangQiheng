#include <stdio.h>

int a[10000];

int main()
{
	int n, digit = 1, temp, i, j, carry;
	scanf("%d", &n);
	a[0] = 1;
	for (i = 2; i <= n; i++)  
	{
		carry = 0;
		for (j = 1; j <= digit; j++)
		{
			temp = a[j-1] * i + carry; 
			a[j-1] = temp % 10; 
			carry = temp / 10; 
		}
		while (carry)
		{
			a[++digit - 1] = carry % 10;
			carry = carry / 10; 
		}
	}
	for (j = digit; j >= 1; j--)
		printf("%d", a[j-1]);

	return 0;
}
