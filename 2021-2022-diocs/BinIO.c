#include<stdio.h>
#include<stdlib.h>

typedef struct student{
    int xh;
    char name[20];
    int math_score;
    int english_score;
    int chinese_score;
}Stu;

void text_to_bin();
void bin_to_text();
/**
 *代码实例是将文本文件写入二进制文件，然后从二进制文件中读取，再写入到
 *文本文件中去
 */
int main(int argc,char *argv[]){
    if(argc !=4 ){
        printf("缺少参数\n");
        exit(EXIT_FAILURE);
    }
    text_to_bin(argv);
    bin_to_text(argv);
    return  0;
}


void text_to_bin(char *argv[]){
    FILE *source_file_pointer;
    FILE *des_file_pointer;
    Stu stu = {1,"demo",0,0,0};
    source_file_pointer = fopen(argv[1],"r");
    if(source_file_pointer == NULL){
        printf("open text source file failed\n");
        exit(EXIT_FAILURE);
    }

    des_file_pointer = fopen(argv[2],"wb");
    if(des_file_pointer == NULL){
        printf("open bin source file failed\n");
        exit(EXIT_FAILURE);
    }

    while(fscanf(source_file_pointer,"%d %s %d %d %d",&stu.xh,stu.name,&stu.math_score,&stu.english_score,&stu.chinese_score) != EOF ){
        fwrite(&stu,sizeof(stu),1,des_file_pointer);
    }
    int source_close_result = fclose(source_file_pointer);
    if(source_close_result == EOF){
        printf("close source file failed\n");
        exit(EXIT_FAILURE);
    }else{
        printf("close source file success\n");
    }

    int des_close_result = fclose(des_file_pointer);
    if(des_close_result == EOF){
        printf("close des file  failed\n");
        exit(EXIT_FAILURE);
    }else{
        printf("close des file success\n");
    }

}


void bin_to_text(char *argv[]){
    FILE *bin_source_file_pointer;
    FILE *text_des_file_pointer;
    Stu stu = {1,"z",0,0,0};
    bin_source_file_pointer = fopen(argv[2],"rb");
    if(bin_source_file_pointer == NULL){
        printf("open bin_source_file failed\n");
        //perror(argv[2]);
        exit(EXIT_FAILURE);
    }else{
        printf("open bin_source_file success\n");
    }

    text_des_file_pointer = fopen(argv[3],"w");
    if(text_des_file_pointer == NULL){
        printf("open text des file failed\n");
        perror(argv[3]);
        exit(EXIT_FAILURE);
    }else{
        printf("open text_des file success\n");
    }

    while(fread(&stu,sizeof(stu),1,bin_source_file_pointer)){
        fprintf(text_des_file_pointer,"%d %s %d %d %d\n",stu.xh,stu.name,stu.math_score,stu.english_score,stu.chinese_score);
    }

    int bin_source_file_close_result = fclose(bin_source_file_pointer);
    if(bin_source_file_close_result == EOF){
        printf("close bin_source_close_file error\n");
        exit(EXIT_FAILURE);
    }else{
        printf("close bin_source_close_file success\n");
    }
    int text_des_file_close_result = fclose(text_des_file_pointer);
    if(text_des_file_close_result == EOF){
        printf("close text_des_file error\n");
        exit(EXIT_FAILURE);
    }else{
        printf("close text_des_file success\n");
    }
}
