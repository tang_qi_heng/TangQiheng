#include <stdio.h>
#include <string.h>
#include <time.h>

int main()
{
    time_t timep;
    struct tm *p;

    time(&timep);
    p = localtime(&timep);

    printf("%d-%d-%d %d:%d:%d\n", (1900 + p->tm_year), ( 1 + p->tm_mon), p->tm_mday,
                                (p->tm_hour + 12), p->tm_min, p->tm_sec);

    return 0;
}
