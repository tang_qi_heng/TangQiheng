#include <signal.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
int count = 0;
struct itimerval t;
time_t start,end ;
void timer_handler(int sig){
      end =time(NULL);
      printf("timer_handler :  signal %d   count=%d  , diff: %ld \n",sig, ++count,end -start);
      start =  end;
      if( count >= 8){
          printf("cancel timer \n");
          t.it_value.tv_sec  =  0 ;
          t.it_value.tv_usec    =    0;
          setitimer(ITIMER_VIRTUAL, &t , NULL);
      }
}
 
int  main(){
      struct itimerval timer ;
      signal (SIGVTALRM ,timer_handler);
      timer.it_value.tv_sec =  0;
      timer.it_value.tv_usec  = 100000;
      //every 1s afterward
      timer.it_interval.tv_sec = 1;
      timer.it_interval.tv_usec = 0;
      // start a virtual itimer
      start = time(NULL);
      setitimer( ITIMER_VIRTUAL , &timer ,NULL );
      printf("press Ctrl + C  to terminate \n");
      while(1);
}
