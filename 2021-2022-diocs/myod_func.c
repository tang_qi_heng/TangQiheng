#include "myod.h" 

void Change(int LJ,int tx[],char tx2[])
{
    dToH(LJ,tx);
    int i;
    for(i=0; i<8; i++)
    {
        tx2[i] = intToChar(tx[i]);
    }
}
void dToH(int H,int tx[])//十进制转十六进制，但此时仍用int存储
{
    if(H>536870911)
    {
        printf("字符数太大，超出限制\n");
        exit (0);
    }
    int i=7;
    for(; i>0;)
    {
        tx[i] = H%16;
        H = H/16;
        i--;
    }
}
char intToChar(int t)//十六进制，把int[]转为char[]
{
    if((t>=0)&&(t<10))
    {
        return t+48;
    }
    else
    {
        return t+87;
    }
}
int count(int *tSY,int *Dan,int *LJ,int *SY,int *num)//计数并判定是否终止
{
    *tSY = *tSY-*Dan;
    printf("\n");
    if(*SY>=16)
    {
        *LJ = *LJ+16;
    }
    else if((*SY>0)&&(*SY<16))
    {
        *LJ = *LJ+*SY;
    }
    else if(*SY==0)
    {
        return 2;
    }
    *SY = *num-*LJ;
    return 1;
}
void outputAscii(char tx2[],int Dan,int LJ,int tx[],char str[])
{
        int tt,j,i2;
        printf("         ");
        for(j=LJ; j<Dan+LJ; j++)
        {
            tt = str[j];
            Change(tt,tx,tx2);
            for(i2=0; i2<8; i2++)
            {
                if(tx2[i2]!='0')break;
            }
            for(;i2<8;i2++)printf("%c",tx2[i2]);
            printf("  \t");
        }
        printf("\n");
}
