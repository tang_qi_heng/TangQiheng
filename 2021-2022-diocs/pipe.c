#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int pd[2], n, i;
char line[256];

int main()
{
	pipe(pd);
	printf("pd=[%d,%d]\n", pd[0], pd[1]);
	if (fork())
	{
		printf("parent %d close pd[0]\n", getpid());
		close(pd[0]);
		while (i++ < 10)
		{
			printf("parent %d writing to pipe\n", getpid());
			n = write(pd[1], "Hello, I'm 20191218tqh!", 32);
			printf("parent %d worte %d bytes to pipe\n", getpid(), n);
		}
		printf("parent %d exit\n", getpid());
	}
	else
	{
		printf("child %d close p[1]\n", getpid());
		close(pd[1]);
		while (1)
		{
			printf("child %d reading from pipe\n", getpid());
			if ((n = read(pd[0], line, 128)))
			{
				line[n] = 0;
				printf("child read %d bytes fron pipe: %s\n", n, line);
			}
			else
				exit(0);
		}
	}
}
 
