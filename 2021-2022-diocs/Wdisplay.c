#include<stdio.h>
#include<stdlib.h>
#define N 3 //字符串个数
#define MAX_SIZE 3007 //字符数组大小，要求每个字符串长度不超过3007
int main (void)
{
    char file_name[30]="txt2.txt";
    char str[MAX_SIZE];
    FILE *fp;
    int i;
    fp=fopen (file_name, "w+") ; //"w+"模式：先写入后读出
    if(NULL==fp)
    {
        printf ("Failed to open the file !\n");
        exit (0);
    }
    printf ("请输入%d个字符串：\n",N);
    for(i=0;i<N;i++)
    {
        printf ("字符串%d:",i+1);
        fgets (str,MAX_SIZE, stdin) ;//从键盘输入字符串，存入str数组中
        fputs (str, fp) ;//把str中字符串输出到fp所指文件中
    }
    rewind (fp); //把fp所指文件的读写位置调整为文件开始处
    while (fgets(str,MAX_SIZE,fp) !=NULL)
    {
        fputs (str, stdout) ; //把字符串输出到屏幕
    }
    fclose(fp);
    return 0;
} 


